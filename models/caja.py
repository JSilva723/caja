# -*- coding: utf-8 -*-

from odoo import models, fields, api, _


class CajaModel(models.Model):
    _name = 'caja.caja' # modulo.modelo
    _description = 'Modelo de movimientos de caja'
    
    TYPE_MOVES = [('entry',_('Entry')),('egress',_('Egress'))]

    type_move = fields.Selection(selection=TYPE_MOVES)
    description = fields.Char(required=True, string=_('Description'), help=_('Move description'))
    entry = fields.Float()
    egress = fields.Float()

    def _compute_balance(self):
      for record in self:
        moves_entries = record.search([('id','<=',record.id),('type_move','=','entry')])
        moves_egresses = record.search([('id','<=',record.id),('type_move','=','egress')])
        
        entries = sum([x.entry for x in moves_entries ])
        egresses = sum([x.egress for x in moves_egresses ])
        record.balance = entries - egresses

    balance = fields.Float(compute=_compute_balance)